import os
import platform
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

"""Class VaccineDistribution generates initial parameter for data to be analyzed from 'vaccinedistribution.csv' file"""


class VaccineDistribution:
    counties_data = []
    counties_ids = []
    county_month = None
    c_avg = None
    all_test = None
    avg_test = None

    def __init__(self, file_name):
        # check that the file exists in parent directory
        if not (os.path.isfile(file_name)):
            print("File does not exist, for vaccinedistribtion.")
            quit()

        # initially tried to create a dtype converter for csv file, currently unsuccessful as data has multiple columns
        # mixed dtypes
        def convert_dtype(x):
            if int(x):
                return x
            if not int(x):
                return str(x)

        # read information, low_memory = False is used a temporary workaround to silence dtype warning
        df = pd.read_csv(file_name, low_memory=False)
        self.split_data_by_county(df)

        # initialize data frame for average vaccine distribution
        avg_structure = {
            'County': [],
            'Average': []
        }
        self.c_avg = pd.DataFrame(avg_structure)

        # initialize data frame for monthly vaccine distribution
        month_structure = {
            'Month': [],
            'Average': []
        }
        self.county_month = pd.DataFrame(month_structure)

    def split_data_by_county(self, df):
        # keep only county names, remove any other summary and column that contains data for statewide, as we are
        # analyzing by county
        only_counties = df[~df['county'].str.contains("Statewide")]

        counties = only_counties['county'].unique()
        for unique_county in counties:
            single_county = only_counties[only_counties['county'] == unique_county]
            self.counties_data.append(single_county)
            self.counties_ids.append(unique_county)

    def compute_avg_distribution(self):
        for each_county in self.counties_ids:
            county = self.counties_data[self.counties_ids.index(each_county)]
            avg_hospital = county['fully_vaccinated'].describe()[['mean']]

            # Add data to dataframe c_avg
            self.c_avg.loc[len(self.c_avg.index)] = [each_county, avg_hospital[0]]

        return self.c_avg

    def compute_month_trend(self, county):
        # generate time frame at the beginning of each month
        time_analysis = pd.date_range(start='3/1/2020', end='5/1/2022', freq='MS')

        # Find the specific county
        index = self.counties_ids.index(county)
        county_under_analysis = self.counties_data[index]

        # convert date column to pandas date type
        county_under_analysis['administered_date'] = pd.to_datetime(county_under_analysis['administered_date'])

        # group data per month
        county_data_month = county_under_analysis.groupby(pd.Grouper(key='administered_date', freq='MS'))

        # only takes the date and fully_vaccinated columns
        hospitalization_month = county_data_month[['administered_date', 'fully_vaccinated']]

        for group in hospitalization_month:
            # print(group)
            # print(group[1].mean())
            month_target = group[0].strftime('%Y-%m-%d')

            # Add data to dataframe average
            self.county_month.loc[len(self.county_month.index)] = [month_target, group[1].mean()[0]]

    def plot_county_data(self):
        # plot average per county
        self.c_avg.plot.bar(xlabel="County", ylabel="Vaccine doses", title="Average Vaccines distributed per CA County",
                            x='County')

    def plot_county_month(self):
        # plot selected county
        self.county_month.plot(xlabel="Month", ylabel="Distributed Vaccines", title='Average Month tendency for',
                               x='Month')


if __name__ == '__main__':
    vaccinedata = VaccineDistribution(os.path.join(os.getcwd(), "data", "vaccinesdistributon.csv"))
    vaccinedata.compute_avg_distribution()
    vaccinedata.compute_month_trend('Los Angeles')

    # vaccinedata.plot_county_data()
    vaccinedata.plot_county_month()

    print('fin vaccinedsit')
