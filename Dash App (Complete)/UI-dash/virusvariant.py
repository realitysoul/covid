import os
import platform
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

class VirusVariant:

    counties_data = []
    counties_ids = []
    #counties_avg = {}
    county_month = None
    c_avg = None
    all_test = None
    avg_test = None


    def __init__(self, file_name):
        # check that the file exist
        if not(os.path.exists(file_name)):
            print("File does not exist, for virusvariant.")
            print(file_name)
            quit()

        # read information
        df = pd.read_csv(file_name)
        self.split_data_by_county(df)

        # initialize data frame for average hospitalization
        avg_structure = {
            'County' : [],
            'Average' : []
        }
        self.c_avg =pd.DataFrame(avg_structure)

        # initialize data frame for monthly hospitalization
        month_structure = {
            'Month' : [],
            'Average' : []
        }
        self.county_month =pd.DataFrame(month_structure)



    def split_data_by_county(self, df):
        # keep only virus names, not totals
        only_counties = df[~df['variant_name'].str.contains("Total")]
        
        counties = only_counties['variant_name'].unique()
        for unique_county in counties:
            single_county = only_counties[only_counties['variant_name'] == unique_county]
            self.counties_data.append(single_county)
            self.counties_ids.append(unique_county)


    def compute_avg_hopitalization(self):
        for each_county  in self.counties_ids:
            county = self.counties_data[self.counties_ids.index(each_county)]
            avg_hospital = county['hospitalized_covid_patients'].describe()[['mean']]
            
            # Add data to dataframe average
            self.c_avg.loc[len(self.c_avg.index)] = [each_county, avg_hospital[0]]



    def compute_month_trend(self, county):
        # generate time frame at the beginning of each month
        time_analysis = pd.date_range(start='3/1/2020', end='5/1/2022', freq='MS') #MS=monthly start, M=month end by default

        # Find the specific county
        index = self.counties_ids.index(county) #finds index value of county in counties_ids
        county_under_analysis = self.counties_data[index] #calls the value using ^ index

        # convert date column to pandas date type
        county_under_analysis['date'] = pd.to_datetime(county_under_analysis['date'])

        # group data per month
        county_data_month = county_under_analysis.groupby(pd.Grouper(key='date', freq='MS'))

        # only takes the date and hospitalized_covid_patients' columns
        hospitalization_month = county_data_month[['date', 'percentage']]

        for group in hospitalization_month:
            # print(group)
            # print(group[1].mean())
            month_target = group[0].strftime('%Y-%m-%d')

            # Add data to dataframe average
            self.county_month.loc[len(self.county_month.index)] = [month_target, group[1].mean()[0]]
            
        return(self.county_month)





    def plot_county_data(self):
        # plot average per county
        self.c_avg.plot.bar(xlabel = "County", ylabel = "Hospitalized", title="Average Hospitalization per CA County",
                              x = 'County')



    def plot_county_month(self):
        # plot selected county
        self.county_month.plot(xlabel = "Month", ylabel = "Hospitalized", title='Average Month tendency for',
                          x = 'Month')



if __name__ == '__main__':

    virusdata = VirusVariant(os.path.join(os.getcwd(), "data", "covid-19-variant-data-3.csv"))
    virusdata.compute_month_trend('Epsilon')
    virusdata.compute_month_trend('Beta')


    virusdata.plot_county_month()


    print('fin virusvariant')


