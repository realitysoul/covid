import os
import platform
import pandas as pd
from dash import Dash, html, Input, Output, dash_table, dcc

import hospitalAdmissions as ha
import vaccinedistrb as vd
import plotly.express as px
import avgrecovery as avgr
import virusvariant as vivir


"""

Final Project for BME-133 Spring 2022
Module:  BME133-dash

Primary Author :Daniela Vivanco

Edited by: Troy Calilong, Zachariah Holder, Nicole Polo

Summary:    This module is the main control & graphic user interface to show the data analyzed
            for the final project BME-133 Spring 2022

            Assumptions:    The user interfaces done as web page
                            DASH framework is used in conjunction with python
"""

# Menu items that are available

bme133_analysis_available = ['Hospital Admissions', 'Average Recovery',
                             'Vaccine Cumulative Distribution', 'Hospital Admissions Timeline',
                             'Vaccine Timeline Distribution',
                             'Virus Variants Timeline']
#from app_contents import make_layout
app = Dash(__name__)


# Dash layout - please refer to presentation for details
app.layout = html.Div(children=[
    html.Div(children=[
        html.H1(children='BME-133 Final Project Sping 2022  Topic: COVID Analysis'),
        html.H2(children='Team Members'),
        html.Ul(children=[
            html.Li('Daniela Vivanco'),
            html.Li('Zachariah Holder'),
            html.Li('Troy Calilong'),
            html.Li('Nicole Polo'),

        ]
        ),
        html.Hr()
    ]
    ),
    html.Div(children=[
        html.Div(children=[
            html.P('NOTE: Available Analysis are only for CA from 1/5/2022 to 5/5/2022'),
            dcc.Dropdown(bme133_analysis_available, bme133_analysis_available[0],
                            id='analysis_type')
        ]
        ),

        html.Div(children=[
            # html.P(children='', id ='analysis_data')
            dcc.Graph(id="analysis_data")
        ]
        ),

    ]
    )
]
)


@app.callback(
        Output('analysis_data', 'figure'),
        Input('analysis_type', 'value')
    )
def process_selection(item):
    """
    Compute desired analysis based on the menu item selected as response of dropmenu change
    :param  item - this is the value received from the drop menu change
    :return analyzed information return the form of a figure,
            this figure is rendered using dash component graph
    """

    if item == bme133_analysis_available[0]:
        hospitaldata = ha.HospitalAdmissionAnalysis(os.path.join(os.getcwd(), "data", "covid19hospitalbycounty.csv"))
        avg = hospitaldata.compute_avg_hopitalization()
        fig = px.bar(avg, title="Average Hospitalization per CA County",
                              x = 'County', y = 'Average')

    elif item == bme133_analysis_available[1]:
        recoverydata = avgr.AvgRecovery(os.path.join(os.getcwd(), "data", "CovidRecoveryCAData.csv"))
        avg = recoverydata.compute_avg_recovery()
        fig = px.bar(avg, title="Average Recovery time per CA County",
                        x = 'County', y = 'Average')

    elif item == bme133_analysis_available[2]: #cumulative vaccine dsitrubtion
        vaccinedata = vd.VaccineDistribution(os.path.join(os.getcwd(), "data", "vaccinesdistributon.csv"))
        avg = vaccinedata.compute_avg_distribution()
        fig = px.bar(avg, title="Average Vaccine Distribution per CA County (By Average of Fully Vaccinated Individuals)",
                        x = 'County', y = 'Average')
    
    if item == bme133_analysis_available[3]:
        hospitaldata = pd.read_csv(os.path.join(os.getcwd(), "data", "covid19hospitalbycounty.csv"))
        fig=px.line(hospitaldata,x='todays_date',y='hospitalized_covid_confirmed_patients',
                    color='county', title='Hospitalization Timeline by County',
                    labels={'todays_date':'Date','hospitalized_covid_confirmed_patients':'Confirmed COVID Hospitilizations'})

    elif item == bme133_analysis_available[4]: #vaccine timeline distribution
        vaccine_distribution_data=pd.read_csv(os.path.join(os.getcwd(), "data", "vaccinesdistributon.csv"))
        fig=px.line(vaccine_distribution_data, x='administered_date', y='fully_vaccinated',color='county',
                    title='Vaccine Distribution Timeline',
                    labels={'administered_date':'Date','fully_vaccinated':'Fully Vaccinated Population'}) #just aesthetic fixes

    elif item == bme133_analysis_available[5]:
        Variant_data = pd.read_csv(os.path.join(os.getcwd(), "data", "covid-19-variant-data-3.csv"))
        fig=px.line(Variant_data,x='date',y='specimens_7d_avg', color='variant_name',
                    title='Specimens 7-day infection average versus Date, for California',
                    labels={'date':'Date','specimens_7d_avg':'7-Day Infection Average'}) #just aesthetic fixes
    return(fig)

if __name__ == '__main__':

    app.run_server(debug=True)



print('fin dash')


