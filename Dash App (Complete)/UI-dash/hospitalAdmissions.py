import os
import platform
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

"""
Final Project for BME-133 Spring 2022
Module:  Hospital Admission
Author:  Daniela Vivanco

Summary:    This module reads the information from a CSV file and
            separate the data per county and time
            Based on these two sets of information the average time
            is computed, one per county and other per month
"""


class HospitalAdmissionAnalysis:
    counties_data = []
    counties_ids = []
    # counties_avg = {}
    county_month = None
    c_avg = None
    all_test = None
    avg_test = None

    def __init__(self, file_name):
        """
        Crate a HospitalAdmissionAnalysis handler
        :param: file_name   Desired file name to read information.
                            expecting full path
        """
        # check that the file exist
        if not (os.path.isfile(file_name)):
            print("File does not exist, for hospital admissions.")
            quit()

        # read information
        df = pd.read_csv(file_name)
        self.split_data_by_county(df)

        # initialize data frame for average hospitalization
        avg_structure = {
            'County': [],
            'Average': []
        }
        self.c_avg = pd.DataFrame(avg_structure)

        # initialize data frame for monthly hospitalization
        month_structure = {
            'Month': [],
            'Average': []
        }
        self.county_month = pd.DataFrame(month_structure)

    def split_data_by_county(self, df):
        """
        Split data per county
        :param      raw data coming from file
        :return     the result is saved in a global variable within the class
                    counties_ids    - only county names
                    counties_data   - all the data per county
        """
        counties = df['county'].unique()
        for unique_county in counties:
            single_county = df[df['county'] == unique_county]
            self.counties_data.append(single_county)
            self.counties_ids.append(unique_county)

    def compute_avg_hopitalization(self):
        """
        Compute average hospitalizaton
        The average is calculated using the column "hospitalized_covid_patients"
        :param      None. Assuming the global class variable counties_data
        :return     avg hospitalization per county
                    the average is saved as well in a global class variable
                    c_avg
        """
        for each_county in self.counties_ids:
            county = self.counties_data[self.counties_ids.index(each_county)]
            avg_hospital = county['hospitalized_covid_patients'].describe()[['mean']]

            # Add data to dataframe average
            self.c_avg.loc[len(self.c_avg.index)] = [each_county, avg_hospital[0]]

        return (self.c_avg)

    def compute_month_trend(self, county):
        """
        Compute the avg month trend per given county.  The column "today_date" is used
        as key to separate per month.  Month is assumed at the beginning
        :param      county  desired county to be analyzed
        :return     Avg trend per month
                    saved as well in global class variable county_month
        """
        # generate time frame at the beginning of each month
        # time_analys = pd.date_range(start='3/1/2020', end='5/1/2022', freq='MS')

        # Find the specific county
        index = self.counties_ids.index(county)
        county_under_analysis = self.counties_data[index]

        # convert date column to pandas date type
        county_under_analysis['todays_date'] = pd.to_datetime(county_under_analysis['todays_date'])

        # group data per month
        county_data_month = county_under_analysis.groupby(pd.Grouper(key='todays_date', freq='MS'))

        # only takes the date and hospitalized_covid_patients' columns
        hospitalization_month = county_data_month[['todays_date', 'hospitalized_covid_patients']]

        for group in hospitalization_month:
            # print(group)
            # print(group[1].mean())
            month_target = group[0].strftime('%Y-%m-%d')

            # Add data to dataframe average
            self.county_month.loc[len(self.county_month.index)] = [month_target, group[1].mean()[0]]

    def plot_county_data(self):
        """
        Auxiliary function to plot avg data analyzed
        It uses the global class vairable
        c_avg
        """
        # plot average per county
        self.c_avg.plot.bar(xlabel="County", ylabel="Hospitalized", title="Average Hospitalization per CA County",
                            x='County')

    def plot_county_month(self):
        """
        Auxiliary function to plot avg monthly data analyzed
        It uses the global class vairable
        county_month
        """
        # plot selected county
        self.county_month.plot(xlabel="Month", ylabel="Hospitalized", title='Average Month tendency for',
                               x='Month')


if __name__ == '__main__':
    cwd = os.getcwd()
    print(cwd)
    hospitaldata = HospitalAdmissionAnalysis(os.path.join(os.getcwd(), "data", "covid19hospitalbycounty.csv"))
    hospitaldata.compute_avg_hopitalization()
    hospitaldata.compute_month_trend('Los Angeles')

    hospitaldata.plot_county_data()
    # hospitaldata.plot_county_month()

    print('fin')
