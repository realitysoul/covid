from dash import Dash, html, Input, Output, dash_table, dcc

bme133_analysis_available = ['Hospital Admissions', 'Average Recovery',
                             'Vaccine Cumulative Distribution', 'Vaccine Timeline Distribution',
                             'Virus Variants']

def make_layout():
    # Dash layout - please refer to presentation for details
    app.layout = html.Div(children=[
        html.Div(children=[
            html.H1(children='BME-133 Final Project Sping 2022  Topic: COVID Analysis'),
            html.H2(children='Team Members'),
            html.Ul(children=[
                html.Li('Daniela Vivanco'),
                html.Li('Zachariah Holder'),
                html.Li('Troy Calilong'),
                html.Li('Nicole Polo'),

            ]
            ),
            html.Hr()
        ]
        ),
        html.Div(children=[
            html.Div(children=[
                html.P('NOTE: Available Analysis are only for CA from 1/5/2022 to 5/5/2022'),
                dcc.Dropdown(bme133_analysis_available, bme133_analysis_available[0],
                             id='analysis_type')
            ]
            ),

            html.Div(children=[
                # html.P(children='', id ='analysis_data')
                dcc.Graph(id="analysis_data")
            ]
            ),

        ]
        )
    ]
    )
    return layout
